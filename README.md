<h1 align="center">Welcome to Henry MacAfee's Code Exercise 👋</h1>


> Built using Bootstrap 4

## Install

Clone the repo by opening a new terminal and typing  
`git clone git@gitlab.com:hmac/hca-uxcoe-prototype.git`
  
Or simply download and unzip.

## Usage



Either navigate to cloned or unzipped repo folder and open `index.html` in the browser, or run as a live server from something like VSCode.


## Author

👤 **Henry MacAfee**

* GitHub: [@hmac2222](https://github.com/hmac2222)
* LinkedIn: [@https:\/\/www.linkedin.com\/in\/henry-macafee-42887743\/](https://linkedin.com/in/https:\/\/www.linkedin.com\/in\/henry-macafee-42887743\/)

